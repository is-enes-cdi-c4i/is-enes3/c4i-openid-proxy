= Climate4Impact OPENID proxy server

The OPENID relay server starts an instance of this proxy server for every user
that authorizes with ESGF in C4I. As soon as the user is logged in an instance of
this service is started with a base64 encoded `TokenSet` as an environment
variable. This `TokenSet` contains an access and a refresh token. This service
then refreshes this `TokenSet` every 4 minutes (1 minute less than the lifetime
of an access token), so that it always has a valid access token. In addition to
this refresh functionality, this service also contains an expressjs server which
can recieve requests from inside the kubernetes cluster, add authorization and
forward them to the protected endpoint. This means that any pod in the cluster
can reach te protected endpoint (this should be improved by using Network
Policies). For now this service is only used to let the relay server call the
userinfo endpoint, but this can be extended to let workflow pods call protected
data endpoints or let notebook pods call protected WPS endpoints.
