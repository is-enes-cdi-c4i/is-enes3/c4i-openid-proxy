import { Issuer, TokenSet, Client } from 'openid-client';
import express from 'express';
import { promisify } from 'util';

import { ISSUER, ENDPOINTS, C4IORIGIN, CLIENTID, CLIENTSECRET } from './config';

let tokenSet: TokenSet;

const delay = promisify(setTimeout);

const refreshEvery = async (seconds: number, client: Client): Promise<void> => {
  while (true) {
    tokenSet = await client.refresh(tokenSet);
    if (tokenSet.access_token) {
      console.log(
        `Short access_token: ${tokenSet.access_token.substring(0, 7)}`
      );
    }
    await delay(seconds * 1000);
  }
};

const main = async (): Promise<void> => {
  const redirectURI = `${C4IORIGIN}/c4i-frontend/login`;

  const issuer = await Issuer.discover(ISSUER);
  const client = new issuer.Client({
    client_id: CLIENTID,
    client_secret: CLIENTSECRET,
    redirect_uris: [redirectURI],
    response_types: ['code'],
  });

  if (!process.env.TOKENSET) {
    throw Error('No token set found in environment.');
  }

  tokenSet = new TokenSet(
    JSON.parse(Buffer.from(process.env.TOKENSET, 'base64').toString('binary'))
  );
  refreshEvery(4 * 60, client).catch((err) => console.error(err));

  const app = express();

  app.get(ENDPOINTS.userinfo_endpoint, (req, res) => {
    console.log(tokenSet);
    client
      .userinfo(tokenSet)
      .then((r) => res.send(r))
      .catch((err) => console.error(err));
  });

  app.listen(4000, () => console.log('App listening...'));
};

main().catch((err) => console.error(err));
