export const ISSUER = 'https://auth-test.ceda.ac.uk/auth/realms/esgf';

const BASEPATH = '/protocol/openid-connect';

export const ENDPOINTS = {
  authorization_endpoint: `${BASEPATH}/auth`,
  jwks_uri: `${BASEPATH}/certs`,
  token_endpoint: `${BASEPATH}/token`,
  introspection_endpoint: `${BASEPATH}/token/introspect`,
  userinfo_endpoint: `${BASEPATH}/userinfo`,
  end_session_endpoint: `${BASEPATH}/logout`,
};

export const C4IORIGIN = process.env.C4I_ORIGIN || '';
export const CLIENTID = process.env.CLIENT_ID || '';
export const CLIENTSECRET = process.env.CLIENT_SECRET || '';
